<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
 
class AppController extends Controller
{

    /**
     * @Route("/", name="home")
     */
    public function homeAction(Request $request)
    {
        return $this->render('home.html.twig');
    }

    /**
     * @Route("/plan", name="plan")
     */
    public function planAction(Request $request)
    {
        return $this->render('plan.html.twig');
    }

    /**
     * @Route("/emplois", name="emplois")
     */
    public function emploisAction(Request $request)
    {
        return $this->render('emplois.html.twig');
    }

    /**
     * @Route("/enseignement-formation", name="enseignement_formation")
     */
    public function enseignementFormationAction(Request $request)
    {
         return $this->render('enseignement_formation.html.twig');
    }

    /**
     * @Route("/international", name="international")
     */
    public function internationalAction(Request $request)
    {
         return $this->render('international.html.twig');
    }

    /**
     * @Route("/emplois-valider", name="emplois_valider")
     */
    public function emploisValiderAction(Request $request)
    {
         return $this->render('emplois_valider.html.twig');
    }


}