<?php
namespace App\Controller;

use App\Entity\JobCategory;
use App\Entity\Studies;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

 
class SurveyController extends Controller
{

    /**
     * @Route("/questionnaire-1", name="questionnaire-1")
     */
    public function surveyAction(Request $request)
    {
        $jobCategories = $this->getDoctrine()->getRepository(JobCategory::class)->findAll();
        return $this->render(
            'survey/firstSurvey.html.twig',
            array(
                'jobCategories' => $jobCategories
            )
        );
    }

    /**
     * @Route("/questionnaire-2", name="questionnaire-2")
     */
    public function surveySecondAction(Request $request)
    {
        $studies = $this->getDoctrine()->getRepository(Studies::class)->findAll();
        return $this->render(
            'survey/secondSurvey.html.twig',
            array('studies' => $studies)
        );
    }

   
}