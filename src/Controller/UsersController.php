<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Service\UsersManager;
 
class UsersController extends Controller
{

    /**
     * @Route("/inscription", name="user_inscription")
     */
    public function inscriptionUserAction(Request $request)
    {
        return $this->render('UserViews/inscription.html.twig');
    }

    /**
     * @Route("/inscription-valide", name="user_inscription_valid", options={"expose"=true})
     */
    public function inscriptionValidUserAction(Request $request)
    {
        return $this->render('UserViews/inscription_valid.html.twig');
    }

    /**
     * @Route("/register-user", name="user_register", options={"expose"=true})
     */
    public function registerUserAction(Request $request)
    {
        $usersManager = $this->container->get(UsersManager::class);
        $response = $usersManager->registerUser($request->request);

        return new JsonResponse($response);
    }

    /**
     * @Route("/connexion", name="user_connexion", options={"expose"=true})
     */
    public function connexionUserAction(AuthenticationUtils $helper): Response
    {
        return $this->render('UserViews/connexion.html.twig', [
            'last_username' => $helper->getLastUsername(),
            'error' => $helper->getLastAuthenticationError(),
        ]);
    }
 
    /**
     *
     * @Route("/deconnexion", name="user_deconnexion")
     */
    public function logout(): void
    {
        throw new \Exception('This should never be reached!');
    }


    /**
     * @Route("/password-forget", name="password_forget")
     */
    public function passwordForgetAction(Request $request)
    {
         return $this->render('UserViews/password_forget.html.twig');
    }

    /**
     * @Route("/profil", name="profil")
     */
    public function profilAction(Request $request)
    {
         return $this->render('UserViews/profil.html.twig');
    }
}