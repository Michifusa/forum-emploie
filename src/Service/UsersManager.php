<?php
namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\Users;
use App\Entity\Sexe;

class UsersManager{

	private $entityManager;

    public function __construct(EntityManagerInterface $entityManagerInterface, ContainerInterface $container, UserPasswordEncoderInterface $passwordEncoder)
    {
    	$this->em = $entityManagerInterface;
        $this->container = $container;
        $this->root = $this->container->get('kernel')->getRootDir();
        $this->user = $this->container->get('security.token_storage')->getToken()->getUser();
        $this->passwordEncoder = $passwordEncoder;
        $this->repositoryUser = $this->em->getRepository(Users::class);
        $this->repositorySexe = $this->em->getRepository(Sexe::class);
    }

    public function registerUser($data)
    {
        $userList = $this->repositoryUser->findAll();
        $reponse = array();
        $response['error'] = false;

        foreach ($userList as $user) {

            //Regarde si l'username et l'email ne sont pas déja uttilisés

            if(strtolower($user->getUsername()) == strtolower($data->get('username'))){
                $response['error'] = true;
                $response['message'][] = 'username_already_use';
            }

            if($user->getEmail() == $data->get('email')){
                $response['error'] = true;
                $response['message'][] = 'email_already_use';
            }

        }

        //Regarde si l'username est défini

        if(!$data->get('username')){
            $response['error'] = true;
            $response['message'][] = 'username_not_defined';
        }

        //Regarde si l'email recupéré est défini

        if(!$data->get('email')){
            $response['error'] = true;
            $response['message'][] = 'email_not_defined';
        }

        //Regarde si l'email recupéré en est bien une

        if(!filter_var($data->get('email'), FILTER_VALIDATE_EMAIL)){
            $response['error'] = true;
            $response['message'][] = 'email_not_adress';
        }

        //Regarde si le nom est défini

        if(!$data->get('user_nom')){
            $response['error'] = true;
            $response['message'][] = 'nom_not_defined';
        }

        //Regarde si le prenom est défini

        if(!$data->get('user_prenom')){
            $response['error'] = true;
            $response['message'][] = 'prenom_not_defined';
        }

        //Regarde si le mot de passe est défini

        if(!$data->get('password')){
            $response['error'] = true;
            $response['message'][] = 'password_not_defined';
        }

        //Regarde si le mot de passe contient au minimum 10 caractères

        if (strlen($data->get('password')) < 10) {
            $response['error'] = true;
            $response['message'][] = 'password_length';
        }

        //Regarde si le mot de passe contient au minimum 1 chiffre

        if (!preg_match("#[0-9]+#", $data->get('password'))) {
            $response['error'] = true;
            $response['message'][] = 'password_number';
        }

        //Regarde si le mot de passe contient au minimum 1 lettre

        if (!preg_match("#[a-zA-Z]+#", $data->get('password'))) {
            $response['error'] = true;
            $response['message'][] = 'password_letter';
        }     

        //Regarde si la confirmation mot de passe est défini

        if(!$data->get('confirm_password')){
            $response['error'] = true;
            $response['message'][] = 'confirm_password_not_defined';
        }

        //Regarde si les deux mot de passe correspondent

        if($data->get('password') != $data->get('confirm_password')){
            $response['error'] = true;
            $response['message'][] = 'password_not_confirm_password';
        }

        //Créé l'utilisateur

        if(!$response['error']){
            $newUser = new Users();
            
            $password = $this->passwordEncoder->encodePassword($newUser, $data->get('password'));
            $newUser->setPassword($password);

            $newUser->setRoles(['ROLE_USER']);
            $newUser->setUserNom($data->get('user_nom'));
            $newUser->setUserPrenom($data->get('user_prenom'));
            $newUser->setUsername($data->get('username'));
            $newUser->setUserTel($data->get('user_tel'));
            $newUser->setUserAge($data->get('user_age'));
            $newUser->setUserSexe($this->repositorySexe->findOneBy(['sexeCode'=>$data->get('user_sexe')]));
            $newUser->setEmail($data->get('email'));

            $this->em->persist($newUser);
            $this->em->flush();
        }

        return $response;
    }
}