<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SexeRepository")
 */
class Sexe
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sexeLabel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sexeCode;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Users", mappedBy="userSexe")
     */
    private $sexeUser;

    public function __construct()
    {
        $this->sexeUser = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSexeLabel(): ?string
    {
        return $this->sexeLabel;
    }

    public function setSexeLabel(string $sexeLabel): self
    {
        $this->sexeLabel = $sexeLabel;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getSexeUser(): Collection
    {
        return $this->sexeUser;
    }

    public function addSexeUser(Users $sexeUser): self
    {
        if (!$this->sexeUser->contains($sexeUser)) {
            $this->sexeUser[] = $sexeUser;
            $sexeUser->setUserSexe($this);
        }

        return $this;
    }

    public function removeSexeUser(Users $sexeUser): self
    {
        if ($this->sexeUser->contains($sexeUser)) {
            $this->sexeUser->removeElement($sexeUser);
            // set the owning side to null (unless already changed)
            if ($sexeUser->getUserSexe() === $this) {
                $sexeUser->setUserSexe(null);
            }
        }

        return $this;
    }

    public function getSexeCode(): ?string
    {
        return $this->sexeCode;
    }

    public function setSexeCode(string $sexeCode): self
    {
        $this->sexeCode = $sexeCode;

        return $this;
    }
}
