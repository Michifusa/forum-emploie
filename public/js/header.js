let sidenav = $(".sidenav");
let containerParent = $(".container.parent");

$(".container-burger").click(function(){
    if(!sidenav.hasClass("show")){
        sidenav.addClass("show");
        containerParent.addClass("hide");
    }
});

$(".close-menu").click(function(){
    if(sidenav.hasClass("show")){
        sidenav.removeClass("show");
        containerParent.removeClass("hide");
    }
});