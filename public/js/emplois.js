$(document).on('click', '.btn-show-emplois', function(event) {
	if($(this).hasClass('active')){
		$(this).removeClass('active');
		$(this).find('i').removeClass('fa-angle-down').addClass('fa-angle-right');
		$(this).next().hide();
	}else{
		$(this).addClass('active');
		$(this).find('i').removeClass('fa-angle-right').addClass('fa-angle-down');
		$(this).next().show();
	}
});


$(document).on('click', '.btn-plus', function(event) {
	if($(this).hasClass('active')){
		$(this).removeClass('active');
	}else{
		$(this).addClass('active');
	}
});