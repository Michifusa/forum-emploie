/*=======================
	Inscription
=======================*/


$(document).on('click', '.btn-register-user', function(event) {

	var formRegisterUser = $('#formRegisterUser');

	formRegisterUser.find('label.error.error-show').removeClass('error-show');
	formRegisterUser.find('.form-line.error').removeClass('error');

	var formRegisterUser_Data = formRegisterUser.serializeArray();

	response = verifFormRegisterUserBeforeSend(formRegisterUser_Data, formRegisterUser);

	if(!response.error){
		$.ajax({
			type: "POST",
			url: Routing.generate('user_register'),
			data: $('#formRegisterUser').serialize(),
			success: function(data) {
				if(data.error){

					$.each(data.message, function(index, messageError) {

						var labelError = formRegisterUser.find('label[data-label="'+messageError+'"]');

						if(index == 0){
							labelError.parents('.form-group').find('input').focus();
						}
						
						labelError.addClass('error-show');
						labelError.parents('.form-group').find('.form-line').addClass('error');
					});
					
				}else{

					window.location.href = Routing.generate('user_inscription_valid');

				}
			}
		});
	}
});

function verifFormRegisterUserBeforeSend(formRegisterUser_Data, formRegisterUser) {
	
	var response = [];
	response.error = false;
	response.errorMessage = [];
	var password = '';
	var confirmPassword = '';

	$.each(formRegisterUser_Data, function(index, input) {
		switch(input.name) {
		    case 'username':
		        if(!input.value){
		        	response.error = true;
		        	response.errorMessage.push('username_not_defined');
		        }
		        break;
		    case 'email':
		        if(!input.value){
		        	response.error = true;
		        	response.errorMessage.push('email_not_defined');
		        }

		        var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;

		        if(!regex.test(input.value)){
		        	response.error = true;
		        	response.errorMessage.push('email_not_adress');
		        }
		        break;
		    case 'user_nom':
		        if(!input.value){
		        	response.error = true;
		        	response.errorMessage.push('nom_not_defined');
		        }
		        break;
		    case 'user_prenom':
		        if(!input.value){
		        	response.error = true;
		        	response.errorMessage.push('prenom_not_defined');
		        }
		        break;
		    case 'password':
		        if(!input.value){
		        	response.error = true;
		        	response.errorMessage.push('password_not_defined');
		        }

		        if(input.value < 10){
		        	response.error = true;
		        	response.errorMessage.push('password_length');
		        }

		        var regexNumber = /[0-9]/;

		        if(!regexNumber.test(input.value)){
		        	response.error = true;
		        	response.errorMessage.push('password_number');
		        }

		        var regexLetter = /[a-zA-Z]/;

		        if(!regexLetter.test(input.value)){
		        	response.error = true;
		        	response.errorMessage.push('password_letter');
		        }

		        password = input.value;
		        break;
		    case 'confirm_password':
		        if(!input.value){
		        	response.error = true;
		        	response.errorMessage.push('confirm_password_not_defined');
		        }
		        confirmPassword = input.value;
		        break;
		    case 'user_tel':
		        if(!input.value){
		        	response.error = true;
		        	response.errorMessage.push('tel_not_defined');
		        }
		        confirmPassword = input.value;
		        break;
		    case 'user_age':
		        if(!input.value){
		        	response.error = true;
		        	response.errorMessage.push('age_not_defined');
		        }
		        confirmPassword = input.value;
		        break;
		}
	});

	if(confirmPassword != password){
		response.error = true;
		response.errorMessage.push('password_not_confirm_password');
	}

	$.each(response.errorMessage, function(index, messageError) {

		var labelError = formRegisterUser.find('label[data-label="'+messageError+'"]');

		if(index == 0){
			labelError.parents('.form-group').find('input').focus();
		}
		
		labelError.addClass('error-show');
		labelError.parents('.form-group').find('.form-line').addClass('error');
	});

	return response;
}

/*=======================
	Connexion
=======================*/

$(document).on('click', '.btn-connect-user', function(event) {

	event.preventDefault();
	var formConnectUser = $('#formConnectUser');
	var formConnectUser_Data = formConnectUser.serializeArray();

	formConnectUser.find('label.error.error-show').removeClass('error-show');
	formConnectUser.find('.form-line.error').removeClass('error');

	var response = [];
	response.error = false;
	response.errorMessage = [];

	$.each(formConnectUser_Data, function(index, input) {
		
		switch(input.name) {
		    case '_username':
		       	if(!input.value){
		       		response.error = true;
		        	response.errorMessage.push('username_not_defined');
		       	}
		        break;
		    case '_password':
		        if(!input.value){
		        	response.error = true;
		        	response.errorMessage.push('password_not_defined');
		       	}
		        break;
		}

	});

	$.each(response.errorMessage, function(index, messageError) {

		var labelError = formConnectUser.find('label[data-label="'+messageError+'"]');

		if(index == 0){
			labelError.parents('.form-group').find('input').focus();
		}
		
		labelError.addClass('error-show');
		labelError.parents('.form-group').find('.form-line').addClass('error');
	});

	if(!response.error){
		formConnectUser.submit();
	}

});

function connexionInvalid() {
	alert('Les identifiants sont incorrectes !');
}

$('select').select2({
	minimumResultsForSearch: -1
});

$('input[type="radio"]').iCheck({
	radioClass: 'icheckbox_minimal-red'
});

/*=======================
	Register Step
=======================*/

$(document).on('click', '.btn-show-second-step', function(event) {
	$('.register-first-step').hide();
	$('.register-second-step').show();
});

$(document).on('click', '.btn-show-first-step', function(event) {
	$('.register-first-step').show();
	$('.register-second-step').hide();
});