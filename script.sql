use hyblab;

INSERT INTO job_category (label) VALUES ('Agriculture/Végétal');
INSERT INTO job_category (label) VALUES ('Animation/Sport');
INSERT INTO job_category (label) VALUES ('Banque/Finance/Téléconseil');
INSERT INTO job_category (label) VALUES ('Bâtiment/Travaux Public/Maintenance');
INSERT INTO job_category (label) VALUES ('Commerce/Force de vente');
INSERT INTO job_category (label) VALUES ('Défense/Sécurité/Justice');
INSERT INTO job_category (label) VALUES ('Enseignement/Formation');
INSERT INTO job_category (label) VALUES ('Hôtellerie/Restauration');
INSERT INTO job_category (label) VALUES ('Industrie/Industrie Agroalimentaire');
INSERT INTO job_category (label) VALUES ('International');
INSERT INTO job_category (label) VALUES ('Nettoyage');
INSERT INTO job_category (label) VALUES ('Santé/Action Sociale/Service à la personne');
INSERT INTO job_category (label) VALUES ('Comptabilité/Achat/Administratif/Informatique/Communication');
INSERT INTO job_category (label) VALUES ('Transport/Logistique/Mécanique');


INSERT INTO studies (label) VALUES ('brevet');
INSERT INTO studies (label) VALUES ('CAP/BEP');
INSERT INTO studies (label) VALUES ('Bac');
INSERT INTO studies (label) VALUES ('Bac+2');
INSERT INTO studies (label) VALUES ('Bac+3');
INSERT INTO studies (label) VALUES ('Bac+5');
INSERT INTO studies (label) VALUES ('Bac+8');
INSERT INTO studies (label) VALUES ('Les études c''est pas mon truc');


